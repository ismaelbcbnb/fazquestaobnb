import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BannerComponent } from './componentes/banner/banner.component';
import { FooterComponent } from './componentes/footer/footer.component';
import { CreateClassesComponent } from './componentes/classes/create-classes/create-classes.component';
import { FormsModule } from '@angular/forms';
import { ListClassesComponent } from './componentes/classes/list-classes/list-classes.component';
import { ClassComponent } from './componentes/classes/class/class.component';
import { SubjectsComponent } from './componentes/classes/subjects/subjects.component';
import { HttpClientModule } from '@angular/common/http';
import { ExcluirClassesComponent } from './componentes/classes/excluir-classes/excluir-classes.component';
import { EditarClassesComponent } from './componentes/classes/editar-classes/editar-classes.component';


@NgModule({
    declarations: [
        AppComponent,
        BannerComponent,
        FooterComponent,
        CreateClassesComponent,
        ListClassesComponent,
        ClassComponent,
        SubjectsComponent,
        ExcluirClassesComponent,
        EditarClassesComponent
    ],
    providers: [],
    bootstrap: [AppComponent],
    imports: [
        BrowserModule,
        AppRoutingModule,
        FormsModule,
        HttpClientModule
    ]
})
export class AppModule { }
