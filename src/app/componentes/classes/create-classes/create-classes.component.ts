import { Component } from '@angular/core';
import { Aula } from '../aula';
import { ClassService } from '../class.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-classes',
  templateUrl: './create-classes.component.html',
  styleUrls: ['./create-classes.component.css']
})
export class CreateClassesComponent {
  class: Aula = {
    materia: '',
    aula: '',
    link: '',
    material: ''
  }

  constructor (
    private service: ClassService, 
    private router: Router
  ){}

  createClass(){
    this.service.criar(this.class).subscribe(() => {
      // this.router.navigate(['/listarAula']);
      alert("Salvo com sucesso!");
    })
  }

  cancelar(){
    this.router.navigate(['/listarAula'])
  }
}