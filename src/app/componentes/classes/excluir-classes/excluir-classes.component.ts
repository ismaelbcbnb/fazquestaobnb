import { Component, OnInit } from '@angular/core';
import { Aula } from '../aula';
import { ClassService } from '../class.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-excluir-classes',
  templateUrl: './excluir-classes.component.html',
  styleUrls: ['./excluir-classes.component.css']
})
export class ExcluirClassesComponent implements OnInit {

  aula: Aula = {
    id: '',
    materia: '',
    aula: '',
    link: '',
    material: ''
  }
  constructor(
    private service: ClassService,
    private router: Router,
    private route: ActivatedRoute
    ) { }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id')
    this.service.buscarPorId(id!).subscribe((aula) =>{
      this.aula = aula
    })
  }

  excluirAula(){
    if(this.aula.id){
      this.service.excluir(this.aula.id).subscribe(() => {
        this.router.navigate(['/listarAula']);
        alert("Aula excluída com sucesso!")
      })
    }
  }

  cancelar(){
    this.router.navigate(['/listarAula']);
  }

}
