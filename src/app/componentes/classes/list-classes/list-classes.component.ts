import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list-classes',
  templateUrl: './list-classes.component.html',
  styleUrls: ['./list-classes.component.css']
})
export class ListClassesComponent implements OnInit {

  listaMaterias= [
    {id: 1, nome: '1 - PLATAFORMA - COMO USAR'},
    {id: 2, nome: '2 - EDITAL - MAPA da PROVA e PLANO de ESTUDOS'},
    {id: 3, nome: '3 - TRILHAS - BANCO do NORDESTE - ANALISTA BANCÁRIO'},
    {id: 4, nome: '4 - PORTUGUÊS'},
    {id: 5, nome: '5 - QUESTÕES PORTUGUÊS - CESGRANRIO'},
    {id: 6, nome: '6 - MATEMÁTICA BÁSICA'},
    {id: 7, nome: '7 - MATEMÁTICA AVANÇADA'},
    {id: 8, nome: '8 - QUESTÕES MATEMÁTICA - CESGRANRIO'},
    {id: 9, nome: '9 - MATEMÁTICA FINANCEIRA'},
    {id: 10, nome: '10 - QUESTÕES MATEMÁTICA FINANCEIRA - CESGRANRIO'},
    {id: 11, nome: '11 - CONHECIMENTOS BANCÁRIOS'},
    {id: 12, nome: '12 - QUESTÕES CONHECIMENTOS BANCÁRIOS - CESGRANRIO'},
    {id: 13, nome: '13 - ATUALIDADES DO MERCADO FINANCEIRO'},
    {id: 14, nome: '14 - QUESTÕES ATUALIDADES DO MERCADO FINANCEIRO'},
    {id: 15, nome: '15 - REVISÃO BANCO do NORDESTE - PORTUGUÊS - FERNANDA'},
    {id: 16, nome: '16 - REVISÃO BANCO do NORDESTE - PORTUGUÊS - THAIS'},
    {id: 17, nome: '17 - REVISÃO BANCO do NORDESTE - MATEMÁTICA'},
    {id: 18, nome: '18 - REVISÃO BANCO do NORDESTE - MATEMÁTICA FINANCEIRA'},
    {id: 19, nome: '19 - REVISÃO BANCO do NORDESTE - CONHECIMENTOS BANCÁRIOS'},
    {id: 20, nome: '20 - REVISÃO ESPECIAL - MATEMÁTICA FINANCEIRA - MARCELO JARDIM'},
    
  ]


  constructor() { }

  ngOnInit(): void {
  }

}
